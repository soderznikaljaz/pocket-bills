# Pocket Bills

Zimska šola Kotlin 2020

Funkcionalnosti aplikacije Pocket Bills:
* avtentikacija s pomočjo storitve Firebase
* shranjevanje slik računov in ključnih besed na Firebase Storage oz. Firebase Firestore
* prikaz računov v UI
* filtriranje po računih
* delo z računi (prenos, brisanje)

Zaslonske posnetke prikaza delovanja aplikacije najdete v mapi "screens".