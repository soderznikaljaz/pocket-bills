package com.example.pocketbills

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DownloadManager
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.chip.Chip
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.item_bill.view.*
import java.io.File


class BillViewHolder(view: View):RecyclerView.ViewHolder(view) {
    private val imageView = view.billImage
    private val chipGroup = view.cardChipGroup
    private val downloadButton = view.cardDownloadButton
    private val deleteButton = view.cardDeleteButton

    fun bind(racun: Racun, context: Context){
        // nastavim sliko racuna
        Glide.with(context).load(racun.imageUri).into(imageView)
        // nastavim keywords - chips
        chipGroup.removeAllViews()
        racun.keywords.forEach{
            val chip = Chip(chipGroup.context)
            chip.text = it
            chipGroup.addView(chip)
        }

        // download gumb
        downloadButton.setOnClickListener{
            MainActivity.downloadFile(racun, context)

        }

        // delete gumb
        deleteButton.setOnClickListener {
            showDialog(racun, context)
        }

    }


    private fun showDialog(racun: Racun, context: Context) {

        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.deleteBillTitle)
            .setMessage(R.string.deleteBillDescription)
            .setPositiveButton(R.string.dialogYes) { _, _ ->  deleteItem(racun, context)}
            .setNegativeButton(R.string.dialogNo, null)
            .show()
    }

    private fun deleteItem(racun: Racun, context: Context){
        //delete bill image
        val storageRef: StorageReference = FirebaseStorage.getInstance().reference
        val imageRef: StorageReference = storageRef.child("billImages/" + racun.userId + "_" + racun.timestamp + ".jpg")
        imageRef.delete().addOnSuccessListener {
            // delete bill data
            val db = FirebaseFirestore.getInstance()
            db.collection("bills").document(racun.dbId ?: "")
                .delete()
                .addOnSuccessListener {
                    MainActivity.removeBill(adapterPosition)
                    //Toast.makeText(context, R.string.successDeleteBill, Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(context,R.string.failureDeleteBill, Toast.LENGTH_SHORT).show()
                }


        }.addOnFailureListener{
            Toast.makeText(context,R.string.failureDeleteBill, Toast.LENGTH_SHORT).show()
        }
    }
}