package com.example.pocketbills

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val i: Intent
        val user: FirebaseUser? = FirebaseAuth.getInstance().currentUser

        i = if (user != null){
            Intent(this, MainActivity::class.java)
        } else{
            Intent(this, LoginActivity::class.java)
        }


        GlobalScope.launch {
            delay(2000L)
            startActivity(i)
            finish()
        }
    }
}
