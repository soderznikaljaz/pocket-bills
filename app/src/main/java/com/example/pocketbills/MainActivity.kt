package com.example.pocketbills

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_IMAGE_CAPTURE = 1
    lateinit var currentPhotoPath: String // absolute path of taken photo

    companion object{
        var racuni: MutableList<Racun> = arrayListOf()
        private lateinit var billAdapter: RecyclerView.Adapter<*>
        private lateinit var viewManager: RecyclerView.LayoutManager

        fun removeBill(position: Int){
            racuni.removeAt(position)
            billAdapter.notifyItemRemoved(position)
            billAdapter.notifyItemRangeChanged(position, racuni.size)
        }

        fun downloadFile(racun: Racun,context: Context){
            val storageRef: StorageReference = FirebaseStorage.getInstance().reference
            val imageRef: StorageReference = storageRef.child("billImages/" + racun.userId + "_" + racun.timestamp + ".jpg")

            val localFile = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "racun_"+racun.timestamp+".jpg")

            imageRef.getFile(localFile).addOnSuccessListener {
                Toast.makeText(context, "Datoteka shranjena\n" + localFile, Toast.LENGTH_LONG).show()
            }.addOnFailureListener{
                Toast.makeText(context, R.string.errorDownloadingBillImage, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbarTop)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        buttonAddBill.setOnClickListener {
            dispatchTakePictureIntent()
        }

        textFilter.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                racuni.filter { it.keywords.any { word ->word.contains(s) }}.let {
                        billAdapter = BillListAdapter(it.toMutableList(), this@MainActivity)
                        billList.adapter = billAdapter
                        billAdapter.notifyDataSetChanged()
                    }
            }
        })

        textFilter.setOnTouchListener(object:View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (event!!.action == MotionEvent.ACTION_DOWN){
                    if (event.rawX >= (textFilter.right - textFilter.compoundDrawables[2].bounds.width())){
                        textFilter.text!!.clear()

                        return true
                    }
                }
                return false
            }
        })

    }

    override fun onStart() {
        super.onStart()
        racuni.clear()
        loadDataFromFirebase()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //Log.e("PATH", currentPhotoPath)
            val racun: Racun = Racun(FirebaseAuth.getInstance().currentUser?.uid)
            racun.imagePath = currentPhotoPath
            val i = Intent(this, BillActivity::class.java)
            i.putExtra("racun", racun)
            startActivity(i)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val resId = item.itemId
        if (resId == R.id.menu_logout){
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
        }

        return super.onOptionsItemSelected(item)
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${FirebaseAuth.getInstance().currentUser?.uid}_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }


    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.example.pocketbills",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    private fun loadDataFromFirebase(){
        //show progress bar
        recycleProgressBar.visibility = View.VISIBLE
        val db = FirebaseFirestore.getInstance()

        db.collection("bills")
            .whereEqualTo("userId", FirebaseAuth.getInstance().currentUser?.uid)
            .get().addOnCompleteListener{task ->
                if (task.isSuccessful){
                    for (document in task.result?.documents ?: arrayListOf()) {
                        val tempRacun: Racun = Racun(document["userId"] as String)
                        tempRacun.timestamp = document["timestamp"]  as Long
                        @Suppress("UNCHECKED_CAST")
                        tempRacun.keywords = document["keywords"] as MutableList<String>
                        tempRacun.imageUri = Uri.parse(document["imageUri"] as String)
                        tempRacun.dbId = document.id

                        racuni.add(tempRacun)
                    }

                    racuni.sortByDescending { racun -> racun.timestamp  }

                    recycleProgressBar.visibility = View.INVISIBLE

                    viewManager = LinearLayoutManager(this)
                    billAdapter = BillListAdapter(racuni, this)
                    billList.apply {
                        layoutManager = viewManager
                        setHasFixedSize(true)
                        adapter = billAdapter

                    }
                }
                else{
                    recycleProgressBar.visibility = View.INVISIBLE
                }
            }
    }
}
