package com.example.pocketbills

import android.net.Uri
import java.io.Serializable

data class Racun(
    val userId: String?

) : Serializable{
    var imagePath: String? = null
    var keywords: MutableList<String> = arrayListOf()
    var timestamp = System.currentTimeMillis()
    var imageUri: Uri? = null
    var dbId: String? = null
}