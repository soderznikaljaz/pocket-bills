package com.example.pocketbills

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class BillListAdapter(var racuni: MutableList<Racun>, var context: Context): RecyclerView.Adapter<BillViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BillViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_bill, parent, false)
    )

    override fun getItemCount(): Int = racuni.size

    override fun onBindViewHolder(holder: BillViewHolder, position: Int) {
        holder.bind(racuni[position], context)
    }


}