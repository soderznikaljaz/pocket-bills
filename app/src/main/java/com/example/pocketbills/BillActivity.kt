package com.example.pocketbills

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import androidx.exifinterface.media.ExifInterface
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.chip.Chip
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_bill.*
import java.io.File


class BillActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bill)

        val racun: Racun = intent.extras?.get("racun") as Racun
        val imageBitmap: Bitmap =  BitmapFactory.decodeFile(racun.imagePath)

        //set correct rotation
        val ei = ExifInterface(racun.imagePath ?: "")
        val orientation: Int = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        val rotatedBitmap: Bitmap?
        rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(imageBitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(imageBitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(imageBitmap, 270f)
            ExifInterface.ORIENTATION_NORMAL -> imageBitmap
            else -> imageBitmap
        }

        imageBill.setImageBitmap(rotatedBitmap)

        val progressView: ViewGroup = (layoutInflater.inflate(R.layout.progressbar_layout, null)) as ViewGroup
        var isProgressShowing = false

         fun showProgressingView() {

            if (!isProgressShowing) {
                isProgressShowing = true;
                (window.decorView.rootView as ViewGroup).apply { addView(progressView) }

            }
        }

        fun hideProgressingView() {
            val v: View = window.decorView.rootView
            val viewGroup: ViewGroup = v as ViewGroup
            viewGroup.removeView(progressView)
            isProgressShowing = false
        }

        buttonBillCancel.setOnClickListener{
            //remove image from internal storage and go back to main activity
            val imgFile = File(racun.imagePath ?: "")
            if (imgFile.exists()) imgFile.delete()
            finish()
        }

        buttonBillSave.setOnClickListener{
            //make screen untouchable and show progress bar
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            showProgressingView()

            //upload image to firebase
            val storageRef: StorageReference = FirebaseStorage.getInstance().reference
            val file: Uri = Uri.fromFile(File(racun.imagePath ?: ""))

            val imageRef: StorageReference = storageRef.child("billImages/" + racun.userId + "_" + racun.timestamp + ".jpg")
            val uploadTask = imageRef.putFile(file).addOnFailureListener{
                //remove image from internal storage and go back to main activity
                val imgFile = File(racun.imagePath ?: "")
                if (imgFile.exists()) imgFile.delete()
                // hide progress bar and make screen touchable again
                hideProgressingView()
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Toast.makeText(this, getString(R.string.errorAddingBill), Toast.LENGTH_SHORT).show()
                finish()
            }

            val urlTask = uploadTask.continueWithTask{ task ->
                if (!task.isSuccessful){
                    task.exception?.let{
                        throw it
                    }
                }
                imageRef.downloadUrl
            }.addOnCompleteListener{task ->
                if (task.isSuccessful){
                    racun.imageUri = task.result
                    //add bill object to firebase
                    val db = FirebaseFirestore.getInstance()
                    val bill: MutableMap<String, Any> = HashMap()
                    bill["userId"] = racun.userId ?: ""
                    bill["keywords"] = racun.keywords
                    bill["timestamp"] = racun.timestamp
                    bill["imageUri"] = racun.imageUri.toString()

                    db.collection("bills")
                        .add(bill)
                        .addOnSuccessListener { documentReference ->
                            // remove Bitmap from internal storage, hide progress bar and make screen touchable again
                            val imgFile = File(racun.imagePath ?: "")
                            if (imgFile.exists()) imgFile.delete()
                            hideProgressingView()
                            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                            Toast.makeText(this, getString(R.string.successAddingBill), Toast.LENGTH_SHORT).show()
                            finish()
                        }
                        .addOnFailureListener { e ->
                            Toast.makeText(this, "FAIL Firestore", Toast.LENGTH_LONG).show()
                            //deleteFile(racun.imagePath)
                            val imgFile = File(racun.imagePath ?: "")
                            if (imgFile.exists()) imgFile.delete()
                            // hide progress bar and make screen touchable again
                            hideProgressingView()
                            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                            Toast.makeText(this, getString(R.string.errorAddingBill), Toast.LENGTH_SHORT).show()
                            finish()
                        }
                }
            }
        }

        buttonAddItem.setOnClickListener{
            if (editItem.text.toString() != "")
            {
                // create chip and place it in chip group
                val chip = Chip(chipGroup.context)
                chip.text = editItem.text.toString()
                chip.isCloseIconVisible = true
                chip.setOnCloseIconClickListener{
                    racun.keywords.remove(chip.text.toString())
                    chipGroup.removeView(chip)
                }

                chipGroup.addView(chip)
                racun.keywords.add(editItem.text.toString())
                editItem.text?.clear()
            }
            else{
                Toast.makeText(this,getString(R.string.errorEmptyEdit), Toast.LENGTH_SHORT).show()
            }


        }
    }

    private fun rotateImage(source: Bitmap, angle: Float):Bitmap{
        val matrix = Matrix()
        matrix.postRotate(angle)
        return  Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }
}
