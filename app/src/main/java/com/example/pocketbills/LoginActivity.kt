package com.example.pocketbills

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        buttonLogin.setOnClickListener {
            loginProgress.visibility = View.VISIBLE
            FirebaseAuth.getInstance().signInWithEmailAndPassword(textUsername.text.toString(),textPassword.text.toString())
                .addOnSuccessListener {
                    loginProgress.visibility = View.INVISIBLE
                    startActivity(Intent(this,MainActivity::class.java))
                    finish()
                }
                .addOnFailureListener {
                    loginProgress.visibility = View.INVISIBLE
                    try {
                        val errorCode = (it as FirebaseAuthException).errorCode

                        if (errorCode == "ERROR_WRONG_PASSWORD") {
                            textUsername.error = getString(R.string.errorWrongPassword)
                        }
                        else if (errorCode == "ERROR_USER_NOT_FOUND" ){
                            textUsername.error = getString(R.string.errorUserNotFound)
                        }
                    }
                    catch(e: Exception){
                    }

                    Toast.makeText(this,"Neuspešna prijava",Toast.LENGTH_SHORT).show()
                }
        }

        buttonRegister.setOnClickListener{
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(textUsername.text.toString(),textPassword.text.toString())
                .addOnSuccessListener {
                    Toast.makeText(this,"Registracija uspešna",Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                .addOnFailureListener{
                    try {

                        when ((it as FirebaseAuthException).errorCode) {
                            "ERROR_EMAIL_ALREADY_IN_USE" -> {
                                textUsername.error = getString(R.string.errorEmailAlreadyInUse)
                            }
                            "ERROR_WEAK_PASSWORD" -> {
                                textUsername.error = getString(R.string.errorWeakPassword)
                            }
                            "ERROR_INVALID_EMAIL" -> {
                                textUsername.error = getString(R.string.errorInvalidEmail)
                            }
                        }
                    }
                    catch(e: Exception){}

                    Toast.makeText(this,R.string.registrationFailed,Toast.LENGTH_LONG).show()
                }
        }
    }

}
